---
stages:
  - lint
  - healthcheck
  - functest
  - performance
  - deploy

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  ANSIBLE_DOCKER_IMAGE:
    registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible
  ANSIBLE_DOCKER_TAG: "2.10"
  CHAINED_CI_INIT: scripts/chained-ci-init.sh
  use_jumphost: "True"
  NOVAULT_LIST: "public/\nresults/"

.syntax_docker: &syntax_docker
  extends: .syntax_checking_tags
  stage: lint
  except:
    - schedules
    - triggers

yaml_linting:
  image: sdesbure/yamllint:latest
  script:
    - "yamllint .gitlab-ci.yml"
  extends: .syntax_docker

ansible_linting:
  image: sdesbure/ansible-lint:latest
  script:
    - "ansible-lint -x ANSIBLE0010 \
        functest.yaml \
        functest_k8s.yaml"
  extends: .syntax_docker

.runner_tags: &runner_tags
  image: ${ANSIBLE_DOCKER_IMAGE}:${ANSIBLE_DOCKER_TAG}
  extends: .ansible_run_tags

##
# Generic Jobs
##
.get_artifact:
  before_script:
    - chmod 700 .
    - . ./${CHAINED_CI_INIT} -a -i inventory/infra
  after_script:
    - ./scripts/clean.sh
  extends: .runner_tags

.prepare_jumphost_and_run:
  stage: healthcheck
  script:
    - ansible-galaxy install -r requirements.yml
    - ansible-playbook ${ansible_verbose} ${VAULT_OPT}
      -i inventory/infra ./functest-jumphost.yaml
    - ansible-playbook ${ansible_verbose} ${VAULT_OPT}
      -i inventory/infra ./functest.yaml
      --extra-vars "functest_tiers=[\"${run_tiers}\"] \
      functest_type=${functest_type}"
  artifacts:
    paths:
      - results/
  extends: .get_artifact
  only:
    refs:
      - triggers

.run:
  stage: functest
  allow_failure: true
  script:
    - ansible-galaxy install -r requirements.yml
    - ansible-playbook ${ansible_verbose} ${VAULT_OPT}
      -i inventory/infra ./functest.yaml
      --extra-vars "functest_tiers=[\"${run_tiers}\"] \
      functest_type=${functest_type}"
  extends: .get_artifact
  only:
    refs:
      - triggers

functest_healthcheck:
  variables:
    run_tiers: healthcheck
    functest_type: openstack
  extends: .prepare_jumphost_and_run
  only:
    variables:
      - $tiers =~ /os_healthcheck/
#      - $tiers == null

functest_smoke:
  variables:
    run_tiers: smoke
    functest_type: openstack
  extends: .run
  only:
    variables:
      - $tiers =~ /os_smoke/

functest_vnf:
  variables:
    run_tiers: vnf
    functest_type: openstack
  extends: .run
  only:
    variables:
      - $tiers =~ /os_vnf/

functest_benchmarking:
  variables:
    run_tiers: benchmarking
    functest_type: openstack
  stage: performance
  extends: .run
  only:
    variables:
      - $tiers =~ /os_benchmarking/

functest_k8s_healthcheck:
  variables:
    run_tiers: healthcheck
    functest_type: k8s
  extends: .prepare_jumphost_and_run
  only:
    variables:
      - $tiers =~ /k8s_healthcheck/
      - $tiers == null

functest_k8s_smoke:
  variables:
    run_tiers: smoke
    functest_type: k8s
  stage: functest
  extends: .run
  only:
    variables:
      - $tiers =~ /k8s_smoke/

functest_k8s_security:
  variables:
    run_tiers: security
    functest_type: k8s
  stage: functest
  extends: .run
  only:
    variables:
      - $tiers =~ /k8s_security/
      - $tiers == null

functest_k8s_cnf:
  variables:
    run_tiers: cnf
    functest_type: k8s
  stage: functest
  extends: .run
  only:
    variables:
      - $tiers =~ /k8s_cnf/
      - $tiers == null

.pages: &pages
  stage: deploy
  extends: .get_artifact
  script:
    - mkdir -p public
    - ansible-playbook ${ansible_verbose} ${VAULT_OPT}
      -i inventory/infra ./functest-pages.yaml
      --extra-vars "functest_type=${functest_type:-unknown}"
    - ls -lrth
    - cp -Rf doc/* public
    - cp -Rf results/* public
  artifacts:
    paths:
      - public/
      - results/
  when: always

pages_os:
  <<: *pages
  variables:
    functest_type: openstack
  only:
    refs:
      - triggers
    variables:
      - $tiers =~ /os_/

pages_k8s:
  <<: *pages
  variables:
    functest_type: k8s
  only:
    refs:
      - triggers
    variables:
      - $tiers =~ /k8s_/
      - $tiers == null
