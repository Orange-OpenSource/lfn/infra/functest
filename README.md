Run Functest on the infrastructure
==================================

Functest
--------

It can be used to test any OpenStack solution.

2 files must be provided:
  * minimalist env files
  * rc file

```
cat env
DEPLOY_SCENARIO=XXX  # if not os-nosdn-nofeature-noha scenario
NAMESERVER=XXX  # if not 8.8.8.8
EXTERNAL_NETWORK=XXX # if not first network with router:external=True
```

```
cat openstack.creds
export OS_AUTH_URL=XXX
export OS_USER_DOMAIN_NAME=XXX
export OS_PROJECT_DOMAIN_NAME=XXX
export OS_USERNAME=XXX
export OS_PROJECT_NAME=XXX
export OS_PASSWORD=XXX
export OS_IDENTITY_API_VERSION=3
```

Several tests require images, so be sure that there are present and reachable.
See https://wiki.opnfv.org/pages/viewpage.action?pageId=13211751

Functest_k8s
------------

It can be used to test kubernetes infrastructure.
It leverages kubernetes conformance test suite.

2 files must be provided:
  * env
  * k8s.creds

```
cat env
INSTALLER_TYPE=XXX
DEPLOY_SCENARIO=XXX
```

```
export KUBERNETES_PROVIDER=local
export KUBE_MASTER_URL=XXX
export KUBE_MASTER_IP=XXX
```

See https://wiki.opnfv.org/pages/viewpage.action?pageId=20742412

Input
-----

  - configuration files:
    - mandatory:
        - vars/vim.yml: only if testing OpenStack
        - inventory/jumphost0_inventory: the ansible inventory for the
          jumphost0 only
        - vars/openstack_openrc: credentials for OpenStack (only if testing
          OpenStack)
        - vars/kube-config: credentials for kubernetes (only if testing
          kubernetes)
  - Environment variables:
    - mandatory:
        - PRIVATE_TOKEN: to get the artifact
        - artifacts_src: the url to get the artifacts
        - OR artifacts_bin: b64_encoded zipped artifacts (tbd)
    - optional:
      - tiers:
          - role: test we want to perform
          - value type: comma separated list or null
          - list values: "os_healthcheck", "os_smoke", "os_vnf", "k8s_healthcheck",
            "k8s_smoke"
          - default: null
      - FUNCTEST_VERSION:
          - role: which version of version to use
          - value: value visible in docker hub ("fraser", "euphrates", "latest"
            at the very least)
          - default: "latest"
      - K8S_CREDS_FILE:
          - role: path to k8s creds file
          - value type: path
          - default: "vars/kube-config"
      - OS_CREDS_FILE:
          - role: path to OpenStack creds file
          - value type: path
          - default: "vars/openstack_openrc"
      - DEPLOY_SCENARIO:
          - role: type of deployment done
          - value type: string
          - default: "k8s" when testing k8s, "os-nosdn-nofeature-ha" when
            testing OpenStack
      - pod:
          - role: name of the pod when we'll insert healtcheck results
          - value type: string
          - default: empty
      - DEPLOYMENT:
          - role: name of the deployment for right tagging when we'll insert
            healtcheck results
          - value type: string
          - default: "rancher" when testing k8s, "kolla" when testing OpenStack
      - TEST_RESULT_DB_URL:
          - role: url of test db api
          - value type: string
          - default: "http://testresults.opnfv.org/test/api/v1/results"


Output
------

  None as of today
