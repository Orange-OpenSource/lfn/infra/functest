#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
""" Module to generate Functest reporting for gitlab pages """

import argparse
import datetime
import logging

import os
import requests
import jinja2

# Logger
logging.basicConfig()
LOGGER = logging.getLogger("Functest-Status")
LOGGER.setLevel("INFO")
# LOGGER.setLevel("DEBUG")

PROXY = {}
# PROXY = {'http': 'socks5h://127.0.0.1:8082',
#          'https': 'socks5h://127.0.0.1:8082'}

# Initialization
URL_PRIVATE_BASE = "http://onap.api.testresults.opnfv.fr/api/v1/results"
URL_PUBLIC_BASE = "http://testresults.opnfv.org/onap/api/v1/results"
URL_BASE = URL_PUBLIC_BASE
REPORTINGDATE = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

if "URL_BASE" is os.environ:
    URL_BASE = os.environ["URL_BASE"]
else:
    URL_BASE = URL_PUBLIC_BASE

# init just connection_check to get the list of scenarios
# as all the scenarios run connection_check
HEALTHCHECK = {'name': 'healthcheck',
               'tests': {'connection_check', 'tenantnetwork1',
                         'tenantnetwork2', 'vmready1', 'vmready2',
                         'singlevm1', 'singlevm2', 'vping_ssh',
                         'vping_userdata', 'cinder_test', 'tempest_smoke'}}

HEALTHCHECK_K8S = {'name': 'healthcheck',
                   'tests': {'k8s_smoke'}}

SMOKE = {'name': 'smoke',
         'tests': {'rally_sanity', 'refstack_defcore',
                   'neutron-tempest-plugin-api', 'patrole',
                   'tempest_full', 'tempest_scenario'}}

BENCHMARKING = {'name': 'benchmarking',
                'tests': {'vmtp', 'shaker', 'rally_jobs', 'rally_full'}}

VNF = {'name': 'vnf',
       'tests': {'cloudify', 'cloudify_ims', 'heat_ims',
                 'vyos_vrouter', 'juju_epc'}}

# list of tests with dedicated reporting page to be referenced
RESULT_URLS = {
    'tempest_smoke': 'healthcheck/tempest_smoke/tempest-report.html',
    'rally_sanity': 'smoke/rally_sanity/rally_sanity.html',
    'refstack_defcore': 'smoke/refstack_defcore/tempest-report.html',
    'neutron-tempest-plugin-api': 'smoke/neutron-tempest-plugin-api/tempest-report.html',
    'patrole': 'smoke/patrole/tempest-report.html',
    'tempest_full': 'smoke/tempest_full/tempest-report.html',
    'tempest_scenario': 'smoke/tempest_scenario/tempest-report.html',
    'vmtp': 'benchmarking/vmtp/vmtp.html',
    'shaker': 'benchmarking/shaker/report.html',
    'rally_jobs': 'benchmarking/rally_jobs/rally_jobs.html',
    'rally_full': 'benchmarking/rally_full/rally_full.html',
    'cloudify_ims': 'vnf/cloudify_ims/ims_test_output.txt',
    'heat_ims': 'vnf/heat_ims/ims_test_output.txt',
    'juju_epc': 'vnf/juju_epc/TestResults.json'
}
# Retrieve the Functest configuration to detect which tests are relevant
# according to the pod, scenario
PERIOD = 8

LOGGER.info("generate Functest reporting page")

PARSER = argparse.ArgumentParser()
PARSER.add_argument('-p', '--pod', help='pod name')
ARGS = PARSER.parse_args()

PODS = ['pod4',
        'onap_daily_pod4_k8s_master',
        'onap_daily_pod4_k8s_elalto']

if ARGS.pod is not None:
    PODS = [ARGS.pod]

LOGGER.info("List of PODS: %s", PODS)

for pod in PODS:
    LOGGER.info("POD: %s", pod)

    TIERS = [HEALTHCHECK, SMOKE, BENCHMARKING, VNF]
    TRENDS = [HEALTHCHECK, SMOKE, BENCHMARKING, VNF]
    TREND_LINE = ""
    SCORE = 0

    # only full suite performed on POD4
    if 'pod4' not in pod:
        TIERS = [HEALTHCHECK]
        TRENDS = [HEALTHCHECK]
    # if k8s in pod name, consider only k8s test suites
    if 'k8s' in pod:
        TIERS = [HEALTHCHECK_K8S]
        TRENDS = [HEALTHCHECK_K8S]

    # Trend
    # *****
    # calculation of the TREND
    SCORE_TREND = 0
    for tier_trend in TRENDS:
        tier_results = []
        nb_tests = 0
        nb_pass = 0
        nb_fail = 0
        score = 0
        for test in tier_trend['tests']:
            url = (URL_BASE + "?case=" + test + "&pod_name=" + pod + "&last=5")
            response = requests.get(url, proxies=PROXY)
            response_json = response.json()
            # Note the 'u' must be used in python 2.7
            # it shall be removed if using python3
            nb_fail = nb_fail + str(response_json).count("criteria': u'FAIL")
            nb_pass = nb_pass + str(response_json).count("criteria': u'PASS")
        try:
            score_trend = round(100 * nb_pass / (nb_pass + nb_fail))
        except ZeroDivisionError:
            score_trend = 0
        LOGGER.debug("Score Trend %s: %s", tier_trend, score_trend)
        tier_trend['score'] = score_trend

    # calculation of the overall SCORE for TREND
    NB_TIERS = 0
    for tier_trend in TRENDS:
        NB_TIERS += 1
        SCORE_TREND = SCORE_TREND + tier_trend['score']
    SCORE_TREND = round(SCORE_TREND / NB_TIERS)

    LOGGER.info("Score Trend: %s", str(SCORE_TREND))

    # calculation of the overall SCORE
    for tier in TIERS:
        tier_results = []
        nb_tests = 0
        nb_pass = 0
        score = 0
        for test in tier['tests']:
            url = (URL_BASE + "?case=" + test + "&period=" + str(PERIOD) +
                   "&pod_name=" + pod + "&last=1")
            LOGGER.debug("url: %s", url)
            response = requests.get(url, proxies=PROXY)
            response_json = response.json()
            response_url = ""
            if test in RESULT_URLS:
                response_url = pod + "/" + RESULT_URLS[test]

            req_result = ""
            try:
                req_result = response_json['results'][0]['criteria']
            except IndexError:
                req_result = None
            result = {'name': test,
                      'result': req_result,
                      'url': response_url}
            LOGGER.debug("result: %s", result)
            nb_tests += 1
            if req_result == "PASS":
                nb_pass += 1
            LOGGER.debug("nb_pass: %s", nb_pass)
            LOGGER.debug("nb_tests: %s", nb_tests)
            score = round(100 * nb_pass / nb_tests)
            LOGGER.debug("score: %s", score)
            tier_results.append(result)

        tier['score'] = score
        tier['results'] = tier_results

    # calculation of the overall SCORE
    NB_TIERS = 0
    for tier in TIERS:
        NB_TIERS += 1
        LOGGER.debug("Score %s", tier)
        SCORE = SCORE + tier['score']
    SCORE = round(SCORE / NB_TIERS)
    LOGGER.info("Score: %s", str(SCORE))

    # calculation of the evolution score versus trend
    if SCORE > 1.05*SCORE_TREND:
        # increasing
        TREND_LINE = "long arrow alternate up icon"
        LOGGER.info("Better status")
    elif SCORE < 0.95*SCORE_TREND:
        # decreasing
        TREND_LINE = "long arrow alternate down icon"
        LOGGER.info("Worst status")
    else:
        # stable
        TREND_LINE = "long arrow alternate right icon"
        LOGGER.info("stable status")

    TEMPLATELOADER = jinja2.FileSystemLoader(".")
    TEMPLATEENV = jinja2.Environment(
        loader=TEMPLATELOADER, autoescape=True)
    TEMPLATE_FILE = ("./template/index-tmpl.html")
    TEMPLATE = TEMPLATEENV.get_template(TEMPLATE_FILE)
    OUTPUT_TEXT = TEMPLATE.render(
        tiers=TIERS,
        pod=pod,
        period=PERIOD,
        date=REPORTINGDATE,
        score=SCORE,
        trend=TREND_LINE)

    FILENAME = "./index-" + pod + ".html"

    with open(FILENAME, "w+") as fh:
        fh.write(OUTPUT_TEXT)
